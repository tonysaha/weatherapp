package com.example.piickme.weatherapp.view;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.example.piickme.weatherapp.R;
import com.example.piickme.weatherapp.adapter.WeatherCityAdapter;
import com.example.piickme.weatherapp.model.CityList;
import com.example.piickme.weatherapp.model.Weather;
import com.example.piickme.weatherapp.model.WeatherApi;
import com.example.piickme.weatherapp.retrofit.ApiUtils;
import com.example.piickme.weatherapp.retrofit.SoService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherCityActivity extends AppCompatActivity implements WeatherCityAdapter.onItemClickListener {

   private RecyclerView recyclerView;
   private RecyclerView.LayoutManager layoutManager;
   private WeatherCityAdapter adapter;
    private List<CityList> cityLists;
    private List<com.example.piickme.weatherapp.model.List> list2;
    private List<WeatherApi> weatherlist;
   private SoService mService;
   private final int MY_PERMISSIONS_REQUEST_INTERNET=2;
   private final int MY_PERMISSIONS_REQUEST_LOCATION=1;

    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_city);
        //ToolBar.......................
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        recyclerView=findViewById(R.id.cityRV);
        cityLists=new ArrayList<>();
        weatherlist=new ArrayList<>();
        list2=new ArrayList<>();

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        permission();

        mService= ApiUtils.getSoService();

        getWeeatherData();

        layoutManager=new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        adapter=new WeatherCityAdapter(cityLists);
        recyclerView.setLayoutManager(layoutManager);


        adapter.setOnItemClickListener(new WeatherCityAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int position) {
               // Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(WeatherCityActivity.this,WeatherDetailsActivity.class);
                intent.putExtra("lat",(String)list2.get(position).getCoord().getLat().toString());
                intent.putExtra("lon",(String)list2.get(position).getCoord().getLon().toString());
                intent.putExtra("city",(String)list2.get(position).getName());
                intent.putExtra("desc",(String)list2.get(position).getWeather().get(0).getDescription());
                intent.putExtra("humidity",list2.get(position).getMain().getHumidity());
                intent.putExtra("speed",list2.get(position).getWind().getSpeed());
                intent.putExtra("max",list2.get(position).getMain().getTempMax()-273.15);
                intent.putExtra("min",list2.get(position).getMain().getTempMin()-273.15);
                intent.putExtra("temp",list2.get(position).getMain().getTemp()-273.15);
                intent.putExtra("icon",(String)list2.get(position).getWeather().get(0).getIcon());
                startActivity(intent);
            }
        });


    }

    private void permission() {
        //Internet Permission..............................................
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.INTERNET)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.INTERNET},
                        MY_PERMISSIONS_REQUEST_INTERNET);

            }
        } else {

        }

       //Locatioon Permision................
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

            }
        } else {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                               Log.d("LOCATION","Lat :"+location.getLatitude()+" Lon :"+location.getLongitude());

                                SharedPreferences pref = getApplicationContext().getSharedPreferences("LOCATION", 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("lat",String.valueOf(location.getLatitude()));
                                editor.putString("lon",String.valueOf(location.getLongitude()));
                                editor.commit();
                            }
                        }
                    });
            Log.d("LOCATION","location permission");


        }


    }


    private void getWeeatherData() {

mService.getWeather("23.68","90.35","50",getString(R.string.weather_api_key)).enqueue(new Callback<WeatherApi>() {
   // public List<Weather> weathers;
    @Override
    public void onResponse(Call<WeatherApi> call, Response<WeatherApi> response) {

        weatherlist= Collections.singletonList(response.body());
        list2=weatherlist.get(0).getList();
        Log.d("WeatherData",String.valueOf(list2.get(0).getName()));

        for (com.example.piickme.weatherapp.model.List listt:list2){

            String cityname=listt.getName();
            String desc=listt.getWeather().get(0).getMain();
            //Temperature convert kelvin to celsius ................................
            double temp=listt.getMain().getTemp();
            int tempC= (int)Math.round(temp - 273.15);

            CityList cityList2=new CityList(cityname,desc,String.valueOf(tempC));
            cityLists.add(cityList2);
        }
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFailure(Call<WeatherApi> call, Throwable t) {

        Log.d("WeatherData2",String.valueOf(t));
    }
});
    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_INTERNET: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }case MY_PERMISSIONS_REQUEST_LOCATION:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request.
        }

    }
}
