package com.example.piickme.weatherapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.piickme.weatherapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class WeatherDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

   private double lat;
   private double lon;
   private TextView dcityTV,ddescTV,dhumidityTV,dmaxTV,dminTV,dtempTV,dspeedTV;
   private ImageView weatherIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);
        //ToolBar.......................
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dcityTV=findViewById(R.id.dcityTV);
        ddescTV=findViewById(R.id.ddescTV);
        dhumidityTV=findViewById(R.id.dhumidittTV);
        dmaxTV=findViewById(R.id.dmaxTV);
        dminTV=findViewById(R.id.dminTV);
        dtempTV=findViewById(R.id.dtempTV);
        weatherIV=findViewById(R.id.weatherIV);
        dspeedTV=findViewById(R.id.dspeedTV);

        //Double price = getIntent().getDoubleExtra("price", 0.00);
        int temp= (int) Math.round(getIntent().getDoubleExtra("temp", 0.00)) ;
        int maxtemp= (int) Math.round(getIntent().getDoubleExtra("max", 0.00)) ;
        int mintemp= (int) Math.round(getIntent().getDoubleExtra("min", 0.00)) ;
        String icon=getIntent().getStringExtra("icon");
        dcityTV.setText(getIntent().getStringExtra("city"));
        ddescTV.setText(getIntent().getStringExtra("desc"));
        dhumidityTV.setText(String.valueOf( getIntent().getDoubleExtra("humidity", 0.00)));
        dspeedTV.setText(String.valueOf( getIntent().getDoubleExtra("speed", 0.00)));
        dmaxTV.setText(String.valueOf(maxtemp)+"°c");
        dminTV.setText(String.valueOf(mintemp)+"°c");
        dtempTV.setText(String.valueOf(temp)+"°c");

        Glide.with(this)
                .load("http://openweathermap.org/img/wn/"+icon+"@2x.png")
                .fitCenter()

                .into(weatherIV);





        lat=Double.valueOf(getIntent().getStringExtra("lat"));
        lon=Double.valueOf(getIntent().getStringExtra("lon"));

        Log.d("Location",getIntent().getStringExtra("lat")+" "+getIntent().getStringExtra("lon"));


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap=googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.clear(); //clear old markers

        CameraPosition googlePlex = CameraPosition.builder()
                .target(new LatLng(lat,lon))
                .zoom(13)
                .bearing(0)
                .tilt(45)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10000, null);

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .title("Spider Man"));

    }
}
