package com.example.piickme.weatherapp.retrofit;

public class ApiUtils {
    public static final String BASE_URL="http://api.openweathermap.org/data/2.5/";


    public static SoService getSoService(){
        return RetrofitClient.getClient(BASE_URL).create(SoService.class);
    }
}
