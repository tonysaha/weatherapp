package com.example.piickme.weatherapp.model;

public class CityList {
   private String cityName;
   private String description;
   private String temp;

    public CityList(String cityName, String description, String temp) {
        this.cityName = cityName;
        this.description = description;
        this.temp = temp;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }
}
