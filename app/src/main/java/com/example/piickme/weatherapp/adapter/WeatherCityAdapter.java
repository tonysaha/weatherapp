package com.example.piickme.weatherapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.piickme.weatherapp.R;
import com.example.piickme.weatherapp.model.CityList;

import java.util.List;

public class WeatherCityAdapter extends RecyclerView.Adapter<WeatherCityAdapter.WeathercityViewHolder> {


    private List<CityList> data;
    private onItemClickListener mListener;

    public interface onItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(onItemClickListener listener){

        this.mListener=listener;
    }

    public WeatherCityAdapter(List<CityList> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public WeathercityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater=LayoutInflater.from(viewGroup.getContext());
        View view=inflater.inflate(R.layout.city_list_layout,viewGroup,false);
        return new WeathercityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeathercityViewHolder weathercityViewHolder, int i) {

        String cityname=data.get(i).getCityName();
        String desc=data.get(i).getDescription();
        String temp=data.get(i).getTemp();

        weathercityViewHolder.cityTV.setText(String.valueOf(cityname));
        weathercityViewHolder.descTV.setText(String.valueOf(desc));
        weathercityViewHolder.tempTV.setText(String.valueOf(temp+"\u00B0c"));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class WeathercityViewHolder extends RecyclerView.ViewHolder{

        TextView cityTV,descTV,tempTV;
        public WeathercityViewHolder(@NonNull View itemView) {
            super(itemView);
            cityTV=itemView.findViewById(R.id.cityNameTV);
            descTV=itemView.findViewById(R.id.descTV);
            tempTV=itemView.findViewById(R.id.tempTV);
//Item click Listener.........................................
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener!=null){
                        int position =getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
