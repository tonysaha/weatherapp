package com.example.piickme.weatherapp.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.piickme.weatherapp.AlartReciver;
import com.example.piickme.weatherapp.MainActivity;
import com.example.piickme.weatherapp.R;

import java.util.Calendar;

public class SplashScreen extends AppCompatActivity {

    private final int SPLAS_TIME_OUT=4000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
//PUsh notification.......................................................................
        AlarmManager alarmManager= (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar=Calendar.getInstance();
//        calendar.add(Calendar.SECOND,5);
        calendar.set(Calendar.HOUR_OF_DAY, 6);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);

        Calendar calendar2=Calendar.getInstance();

        calendar2.set(Calendar.HOUR_OF_DAY, 13);
        calendar2.set(Calendar.MINUTE, 00);
        calendar2.set(Calendar.SECOND, 00);

        Calendar calendar3=Calendar.getInstance();
//        calendar.add(Calendar.SECOND,5);

        calendar3.set(Calendar.HOUR_OF_DAY, 18);
        calendar3.set(Calendar.MINUTE, 00);
        calendar3.set(Calendar.SECOND, 00);


        Intent intent=new Intent(this, AlartReciver.class);
        PendingIntent broadcast=PendingIntent.getBroadcast(this,100,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.setRepeating(AlarmManager.RTC,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,broadcast);
        alarmManager.setRepeating(AlarmManager.RTC,calendar2.getTimeInMillis(),AlarmManager.INTERVAL_DAY,broadcast);

        alarmManager.setRepeating(AlarmManager.RTC,calendar3.getTimeInMillis(),AlarmManager.INTERVAL_DAY,broadcast);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent loginIntent=new Intent(SplashScreen.this, WeatherCityActivity.class);
                startActivity(loginIntent);
                finish();
            }
        },SPLAS_TIME_OUT);


    }
}
