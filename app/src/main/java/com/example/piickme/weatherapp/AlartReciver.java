package com.example.piickme.weatherapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.NotificationTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.piickme.weatherapp.model.CityList;
import com.example.piickme.weatherapp.model.WeatherApi;
import com.example.piickme.weatherapp.model.weatherByLocation.WeatherByLocaation;
import com.example.piickme.weatherapp.retrofit.ApiUtils;
import com.example.piickme.weatherapp.retrofit.SoService;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Handler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlartReciver extends BroadcastReceiver {
    private SoService mService;
    List<WeatherByLocaation> weatherByLocaations;
    @Override
    public void onReceive(Context context, Intent intent) {


        int i=4;
        int r=3+i;

        mService= ApiUtils.getSoService();
        weatherByLocaations=new ArrayList<>();
        getWeatherdata(context,intent);


    }

    private void getWeatherdata(final Context context, final Intent intent) {

        SharedPreferences pref = context.getSharedPreferences("LOCATION", 0);
        String lat=pref.getString("lat",null);
        String lon=pref.getString("lon",null);

         List<com.example.piickme.weatherapp.model.weatherByLocation.WeatherByLocaation> weatherByLocaationList=new ArrayList<>();
        mService.getlocatinWeather(lat,lon,context.getString(R.string.weather_api_key)).enqueue(new Callback<WeatherByLocaation>() {
            @Override
            public void onResponse(Call<WeatherByLocaation> call, Response<WeatherByLocaation> response) {

                weatherByLocaations=Collections.singletonList(response.body());
                Log.d("Alart ",weatherByLocaations.get(0).getMain().getTemp().toString());
                double tempD=(weatherByLocaations.get(0).getMain().getTemp()-273.15);
               int temp=(int)tempD;
               String icon=weatherByLocaations.get(0).getWeather().get(0).getIcon();
               sendNotification(context,intent,temp,icon);



            }

            @Override
            public void onFailure(Call<WeatherByLocaation> call, Throwable t) {
                Log.d("Alart",String.valueOf(t));

            }
        });

    }
    Bitmap bitmap;
    private void sendNotification(final Context context, Intent intent, int temp, String icon) {
        NotificationCompat.Builder builder=new NotificationCompat.Builder(context);

        Notification notification=builder.setContentTitle("Weather App")
                .setContentText("Current Temperature: "+temp+"\u00B0c")
                .setTicker("Today weather")
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setPriority(5)
                .build();
        NotificationManager notificationManager= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

       notificationManager.notify(0,notification);




    }


}
