package com.example.piickme.weatherapp.retrofit;

import com.example.piickme.weatherapp.model.WeatherApi;
import com.example.piickme.weatherapp.model.weatherByLocation.WeatherByLocaation;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SoService {

    @GET("find?")
    Call<WeatherApi> getWeather(@Query("lat") String latitude,
                                @Query("lon") String longitude,
                                @Query("cnt") String cnt,
                                @Query("appid") String appid);

    @GET("weather?")
    Call<WeatherByLocaation> getlocatinWeather(@Query("lat") String latitude,
                                               @Query("lon") String longitude,
                                               @Query("appid") String appid);
}
